require_relative 'rover'

class Controller
  def initialize(file_path)
    @file_path = file_path
    @commands = {}
  end

  def explore
    parse_input
    validate_commands
    launch_rovers @commands[:plateu_size], @commands[:rovers_command_groups]
  end

  private

  def parse_input
    raw_commands = File.open(@file_path) { |f| f.readlines.map(&:chomp) }
    @commands[:plateu_size] = raw_commands.shift.split.map(&:to_i)
    @commands[:rovers_command_groups] = raw_commands.each_slice(2).to_a
  end

  def validate_commands
    unless @commands[:plateu_size].length == 2
      puts 'Wrong plateu size' and return
    end

    puts 'Wrong set of commands' if @commands.length.odd?
  end

  def launch_rovers(plateu_size, rovers_command_groups)
    rovers_command_groups.each do |rovers_command_group|
      Rover.new(plateu_size, rovers_command_group).find_martians
    end
  end
end
