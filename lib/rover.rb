class Rover
  COMPASS_POINTS = ['N', 'E', 'S', 'W']

  def initialize(plateu_size, commands)
    @plateu_size = plateu_size
    @position = commands.first.split
    @moves = commands.last.upcase.split('')
    @x = 0
    @y = 0
    @heading = ''
  end

  def find_martians
    deploy_rover
    make_moves
    report_position 
  end 

  private

  def deploy_rover
    @x = @position.first.to_i
    @y = @position[1].to_i
    @heading = @position.last.upcase
  end

  def make_moves
    @moves.each do |move|
      case move
        when 'M'
          puts 'Reached out the plateu area:' and return false unless move!
        when 'L'
          turn_left!
        when 'R'
          turn_right!
      end
    end
  end

  def report_position
    puts "#{@x} #{@y} #{@heading}"
  end

  def move!
    case @heading
      when 'N'
        @y += 1
      when 'S'
        @y -= 1
      when 'E'
        @x += 1
      when 'W'
        @x -= 1
      else
        puts 'Wrong command'
    end

    @x.between?(0, @plateu_size.first) && @y.between?(0, @plateu_size.last)
  end
  
  def turn_left!
    current_heading_index = COMPASS_POINTS.index(@heading)
    @heading = COMPASS_POINTS[current_heading_index - 1]
  end
  
  def turn_right!
    if @heading == COMPASS_POINTS.last
      @heading = COMPASS_POINTS.first
    else
      current_heading_index = COMPASS_POINTS.index(@heading)
      @heading = COMPASS_POINTS[current_heading_index + 1]
    end
  end
end
